import discord #Discord API
from difflib import get_close_matches #To search close name
import pickle #Data storage (change to SQLite ? can browse easily)
import requests #Request online data
from slpp import slpp #To read lua data from addon
import html #To unescape html text
import os 
import re #Regex
import datetime #For last update date
import logging #TODO replace prints by log

#TODO : add protections

#Global definitions
intents = discord.Intents.default()
intents.members = True
client = discord.Client(intents=intents)

professions = {}
professions["Alchemy"] = ":alembic: Alchimie\n"
professions["Enchanting"] = ":magic_wand: Enchantement\n" #Values for enchanting are negative
professions["Blacksmithing"] = ":hammer: Forge\n"
professions["Mining"] = ":hammer_pick: Minage\n"
professions["Engineering"] = ":bomb: Ingénierie\n"
professions["Tailoring"] = ":thread: Couture\n"
professions["Leatherworking"] = ":long_drum: Travail du cuir\n"
professions["Jewelcrafting"] = ":gem: Joaillerie\n"
professions["Cooking"] = ":bread: Cuisine\n"

quality_color=[0xa0a0a0, 0xffffff, 0x1eff00, 0x0070dd, 0xa335ee, 0xff8000, 0x6fbfd5]

crafts = {} #Dict containing every guild craft
#discord_servername -> Guildname
#				  |-> LastUpdate -> Who -> When
#				  |-> Whitelist -> itemID -> Name(Guild) #TODO
#				  |-> Blacklist -> Name (players that stopped playing for example)
#				  |-> Channels -> The channels where the bot can be used
#				  |-> Updater -> Discord role of the members able to communicate with the bot
#				  |-> Data -> Craftid -> Crafters names
namedict = {} #Dict containing a table name -> id to search items
crafts_infos = {} #Dict containing crafts informations like required materials and profession
#craftid -> Profession
#		|-> Req -> Itemid -> number
#		|-> Quality (int)
reqdict = {} #Dict containing a table id -> name for reagents


#Search the first value corresponding in a dict and return the key
def search_dict(dictionnary, value):
	for i in dictionnary.keys():
		if dictionnary[i] == value:
			return i
	
	#If not found return none
	return None


#Get item/spell name by its ID on wowhead
def get_item_by_number (item, prof):
	if prof == "Enchanting":
		html_page = requests.get('https://fr.tbc.wowhead.com/spell=' + str(-item)).text
		regex = re.search(r"<title>\s*(.*?)\s*Sort", html_page)
	else:
		html_page = requests.get('https://fr.tbc.wowhead.com/item=' + str(item)).text
		regex = re.search(r"<title>\s*(.*?)\s*Obje", html_page)

	if len(regex.group()) <= len("<title>Obje"):
		return "", {}
	else:
		name = html.unescape(regex.group()[7:-7])
		req={}

		regex = re.search(r"\"reagents\":\[\[[0-9,\[\]]*\]\]", html_page)

		reagents = regex.group()[13:-2].split('],[')

		for i in reagents:
			req[int(i.split(',')[0])] = int(i.split(',')[1])

		if prof == "Enchanting":
			qual = -1
		else:
			#The .replace is ugly...
			regex = re.search(r"\"name_frfr\":\"" + re.escape(name.replace('"','\\"')) + r"\",\"quality\":[0-9],", html_page)
			qual = int(regex.group()[-2:-1])

		return name, req, qual


#Update crafts, namedict and crafts_infos database
def update_db(server_name, filename, updater):

	print("Starting DB update for discord", server_name, "...")

	# Decode Guildbook datas to find crafts
	print("Read Guilbook data file...", end='')
	data = slpp.decode(open(filename).read().replace("GUILDBOOK_GLOBAL = ",""))
	print("\rRead Guilbook data file -> OK")

	init_len_crafts = len(crafts[server_name]["Data"].keys())

	temp_data = {}

	ig_guildname = crafts[server_name]["Guildname"]

	player_count = 0
	player_tot = 0

	for name in data["GuildRosterCache"][ig_guildname]:

		player = data["GuildRosterCache"][ig_guildname][name]["Name"]
		player_tot +=1

		proflist=["Cooking"]

		if "Profession1" in data["GuildRosterCache"][ig_guildname][name].keys():
			proflist.append(data["GuildRosterCache"][ig_guildname][name]["Profession1"])
		if "Profession2" in data["GuildRosterCache"][ig_guildname][name].keys():
			proflist.append(data["GuildRosterCache"][ig_guildname][name]["Profession2"])

		prn = 0

		for profession in proflist:

			if profession != "-" and profession in data["GuildRosterCache"][ig_guildname][name].keys():

				prn += 1

				for item in data["GuildRosterCache"][ig_guildname][name][profession]:

					if profession == "Enchanting":
						item = -item

					if item in temp_data.keys():
						temp_data[item]+=[player]
					else:
						temp_data[item]=[player]
						
						#If item not in Spelllistfull.txt, add it and notify
						if item not in namedict.values():
							itemname, req, qual = get_item_by_number(item, profession)
							print("ITEM NOT IN DB :", item, itemname)
							namedict[itemname] = item
							crafts_infos[id]={"Profession" : profession, "Req" : req, "Quality":qual}

							for i in req:
								if i not in reqdict.keys():
									reqdict[i], _, _ = get_item_by_number(i, "")

		player_count+=1*(prn>0)

	crafts[server_name]["Data"] = temp_data
	crafts[server_name]["LastUpdate"] = (updater, datetime.datetime.now().strftime("%d/%m/%y %H:%M:%S"))

	save_datas()

	print("Update successfull!")

	return (len(crafts[server_name]["Data"].keys()) - init_len_crafts), player_count, player_tot


@client.event
async def on_ready():
	print('We have logged in as {0.user}'.format(client))

	for server in client.guilds:
		if server.name not in crafts.keys():
			crafts[server.name] = {"Guildname":"", "LastUpdate":("",""), "Channels":[], "Updater":"", "Data":{}}

	with open('crafts.pkl', 'wb') as f:
		pickle.dump(crafts, f, pickle.HIGHEST_PROTOCOL)


@client.event
async def on_guild_join(server):
	if server.name not in crafts.keys():
			crafts[server.name] = {"Guildname":"", "LastUpdate":("",""), "Channels":[], "Updater":"", "Data":{}}

	with open('crafts.pkl', 'wb') as f:
		pickle.dump(crafts, f, pickle.HIGHEST_PROTOCOL)


@client.event
async def on_message(message):
	#Ignore self messages
	if message.author == client.user:
		return

	#Manually add or remove crafter
	if message.content.startswith('!crafter') and message.channel.type is discord.ChannelType.private:

		"""#MAINTENANCE
		if message.author.name != "Akromha":
			embed=discord.Embed(title="", description= ':tools: Bot en maintenance', color=0x101010)
			await message.channel.send(embed=embed)
			return"""

		for server in client.guilds:
			member = server.get_member(message.author.id)

			if member:
				#And if admin is admin of multiple servers ? -> TODO add the server name in config command !
				if any([i.permissions.administrator for i in member.roles]):
					command = message.content.split(' ',4)

					if len(command) < 2:
						await message.channel.send("Pour supprimer un crafteur dans le bot :\n"
													"> **!crafter remove *Nomdujoueur***\n\n"
													"Pour annuler la supression d'un crafteur dans le bot :\n"
													"> **!crafter unremove *Nomdujoueur***\n\n"
													"Pour afficher les joueurs supprimés du bot :\n"
													"> **!crafter blacklist**\n\n")
													#"Pour ajouter un crafteur dans le bot :\n"
													#"> **!crafter add *Nomdujoueur item_ID guilde***\n"
													#"> L'ID de l'item peut se trouver dans l'URL de wowhead, le nom de guilde est facultatif.\n"
													#"> :tools: Cette fonction n'est pas encore implémentée\n\n"
													#"Pour afficher les joueurs ajoutés manuellement dans le bot :\n"
													#"> **!crafter whitelist**\n"
													#"> :tools: Cette fonction n'est pas encore implémentée")
						return

					if command[1].lower() == "whitelist":
						await message.channel.send("> :tools: Cette fonction n'est pas encore implémentée")
					elif command[1].lower() == "blacklist":
						if len(crafts[server.name]["Blacklist"]) > 0:
							await message.channel.send(', '.join(crafts[server.name]["Blacklist"]))
						else:
							await message.channel.send("Aucun joueur dans la blacklist")
					elif command[1].lower() == "add":
						await message.channel.send("> :tools: Cette fonction n'est pas encore implémentée")
					elif command[1].lower() == "remove" and len(command) == 3:
						crafts[server.name]["Blacklist"].append(command[2].title())
						await message.channel.send(command[2].title() + " ajouté à la blacklist.")
					elif command[1].lower() == "unremove" and len(command) == 3:
						crafts[server.name]["Blacklist"][:] = [x for x in crafts[server.name]["Blacklist"] if not command[2].title()]
						await message.channel.send(command[2].title() + " supprimé de la blacklist.")
					else:
						await message.channel.send("Syntaxe incorrecte")
						return

					save_DB()


	#Configure bot
	if message.content.startswith('!config') and message.channel.type is discord.ChannelType.private:

		"""#MAINTENANCE
		if message.author.name != "Akromha":
			embed=discord.Embed(title="", description= ':tools: Bot en maintenance', color=0x101010)
			await message.channel.send(embed=embed)
			return"""

		for server in client.guilds:
			member = server.get_member(message.author.id)

			if member:
				#And if admin is admin of multiple servers ? -> TODO add the server name in config command !
				if any([i.permissions.administrator for i in member.roles]):
					command = message.content.split(' ',2)
					if len(command) < 2:
						await message.channel.send("Pour configurer le bot, vous devez entrer le nom de la guilde, le(s) canal(aux) que peut utiliser le bot et le nom du rôle utilisé pour mettre à jour la DB.\nLes commandes respectives sont :\n> !config guild *NomDeLaGuilde* (attention aux majuscules et accents)\n> !config channels *canal1,canal-2*\n> !config role *Nom du rôle*\n> !config list (permet d'afficher les valeurs configurées)")
						return

					if command[1].lower() == "list":
						await message.channel.send("Configuration pour le serveur *" + server.name + "*\n**Nom de la guide :** " + crafts[server.name]["Guildname"] + "\n**Canaux utilisés par le bot :** " + ', '.join(crafts[server.name]["Channels"]) + "\n**Role permettant d'update la DB :** " + crafts[server.name]["Updater"] + "\n**Nombre de craft en DB** : " + str(len(crafts[server.name]["Data"])) + "\n**Dernière MàJ** : " + crafts[server.name]["LastUpdate"][1] + " par " + crafts[server.name]["LastUpdate"][0] )
					elif command[1].lower() == "guild":
						crafts[server.name]["Guildname"] = command[2]
					elif command[1].lower() == "channels":
						crafts[server.name]["Channels"] = command[2].replace(" ","").lower().split(',')
						#Check if channels on server
					elif command[1].lower() == "role":
						crafts[server.name]["Updater"] = command[2]
						#Check if role on server
					else:
						await message.channel.send("Syntaxe incorrecte")

					if crafts[server.name]["Updater"] and crafts[server.name]["Channels"] and crafts[server.name]["Guildname"]:
						await message.channel.send("Serveur configuré ! Les personnes ayant le rôle *" + crafts[server.name]["Updater"] + "* peuvent maintenant envoyer le fichier *Guildbook.lua* par MP au bot.\nCe fichier se trouve dans le répertoire d'installation de WoW sous *\_classic\_/WTF/Account/NUMERODECOMPTE/SavedVariables/Guildbook.lua*")

					with open('crafts.pkl', 'wb') as f:
						pickle.dump(crafts, f, pickle.HIGHEST_PROTOCOL)

					return

	#Update DB
	if message.channel.type is discord.ChannelType.private:

		"""#MAINTENANCE
		if message.author.name != "Akromha":
			embed=discord.Embed(title="", description= ':tools: Bot en maintenance', color=0x101010)
			await message.channel.send(embed=embed)
			return"""

		for server in client.guilds:
			member = server.get_member(message.author.id)

			if member:

				if crafts[server.name]["Updater"] in [i.name for i in member.roles]: #or any([i.permissions.administrator for i in member.roles]) ?
					
					if len(message.attachments):
						if message.attachments[0].filename == "Guildbook.lua":

							if not crafts[server.name]["Guildname"]:
								await message.channel.send(":no_entry: Bot non configuré, l'administrateur du serveur doit envoyer *!config* au bot.")
								return

							r = requests.get(message.attachments[0].url)
							
							with open("GB_bot_update_"+server.name.replace(' ','_'),'wb') as output_file:
								output_file.write(r.content)

							await message.channel.send("Guildbook.lua importé")
							await message.channel.send("Mise à jour de la base de données et des noms d'objets...")

							num, player_count, player_tot = update_db(server.name, "GB_bot_update_"+server.name.replace(' ','_'), message.author.name)

							await message.channel.send("Mise à jour de la DB pour le serveur " + server.name + " effectuée\n" + str(num) + " nouveaux items disponibles dans la guilde\n"+ str(player_count) + " artisans trouvés sur "+ str(player_tot) +" personnages en DB")
							return
					elif not crafts[server.name]["Updater"]:
						await message.channel.send(":no_entry: Bot non configuré, l'administrateur du serveur doit MP !config.")
						return

	#Answer query
	elif message.channel.name in crafts[message.channel.guild.name]["Channels"]:
		
		if message.content.startswith('!craft ') and len(message.content.split(' ',1)) > 1:

			"""#MAINTENANCE
			if message.author.name != "Akromha" or message.channel.name != "test":
				embed=discord.Embed(title="", description= ':tools: Bot en maintenance', color=0x101010)
				await message.channel.send(embed=embed)
				return"""

			if not db_ready(message.channel.guild.name):
				return

			item_name_asked = message.content.split(' ',1)[1]

			predicted_names = get_close_matches(item_name_asked, namedict.keys(), n=9, cutoff=0.57)

			if len(predicted_names) > 0:

				await send_craft_infos(message.channel, predicted_names[0])

			else:
				embed=discord.Embed(title="", description= '"' + item_name_asked + '"\n\n> :x: Aucun objet/enchantement correspondant', color=0x801010)
				await message.channel.send(embed=embed)


#Send craft infos
async def send_craft_infos(channel, item_name):

	item_id = namedict[item_name]


	craft_infos = crafts_infos[item_id]
	req=[str(craft_infos["Req"][i])+"x "+reqdict[i] for i in craft_infos["Req"]]
	
	is_enchant = (craft_infos["Profession"] == "Enchanting")

	noone = False
	crafters_list=[]

	if not item_id in crafts[channel.guild.name]["Data"].keys():
		noone = True
	else:
		crafters_list = [x for x in crafts[channel.guild.name]["Data"][item_id] if x not in crafts[channel.guild.name]["Blacklist"]]

	if noone or len(crafters_list) < 1:
		txt = "**[[" + item_name + "]](https://fr.tbc.wowhead.com/" + ["item", "spell"][is_enchant] + "=" + str([1,-1][is_enchant]*item_id) + ")**\n"
		txt +=	professions[craft_infos["Profession"]] + "*(" + ' + '.join(req) + ")*\n\n> :man_shrugging: Aucun artisan trouvé dans la guilde"
		embed = discord.Embed(title="", description= txt, color=0xef3515)
		await channel.send(embed=embed)
		return
	
	crafters = ', '.join(crafters_list)

	txt = "**[[" + item_name + "]](https://fr.tbc.wowhead.com/" + ["item", "spell"][is_enchant] + "=" + str([1,-1][is_enchant]*item_id) + ")**\n"
	txt +=	professions[craft_infos["Profession"]] + "*(" + ' + '.join(req) + ")*\n\n> " + crafters

	embed=discord.Embed(title="", description= txt, color=quality_color[craft_infos["Quality"]])
	await channel.send(embed=embed)


#Load saved datas
def load_datas():

	global namedict
	global crafts_infos
	global crafts
	global reqdict

	if os.path.exists('namedict.pkl') and os.path.exists('crafts_infos.pkl'):
		with open('namedict.pkl', 'rb') as f:
			namedict = pickle.load(f)
			
		with open('crafts_infos.pkl', 'rb') as f:
			crafts_infos = pickle.load(f)
	else:
		create_namedict_crafts_infos()

	if os.path.exists('crafts.pkl'):
		with open('crafts.pkl', 'rb') as f:
			crafts = pickle.load(f)

	if os.path.exists('reqdict.pkl'):
		with open('reqdict.pkl', 'rb') as f:
			reqdict = pickle.load(f)
	else:
		create_reqdict()


#Save datas
def save_datas():
	with open('namedict.pkl', 'wb') as f:
		pickle.dump(namedict, f, pickle.HIGHEST_PROTOCOL)

	with open('crafts.pkl', 'wb') as f:
		pickle.dump(crafts, f, pickle.HIGHEST_PROTOCOL)

	with open('crafts_infos.pkl', 'wb') as f:
		pickle.dump(crafts_infos, f, pickle.HIGHEST_PROTOCOL)

	with open('reqdict.pkl', 'wb') as f:
		pickle.dump(reqdict, f, pickle.HIGHEST_PROTOCOL)


#Save crafts DB
def save_DB():
	with open('crafts.pkl', 'wb') as f:
		pickle.dump(crafts, f, pickle.HIGHEST_PROTOCOL)


#Create namedict from wowhead datas (file Spelllistfull.txt)
def create_namedict_crafts_infos():

	print('Creating namedict and crafts_infos')

	namedict.clear()
	crafts_infos.clear()

	txt = open("Spelllistfull.txt", "r").read()

	ids=[]
	names=[]
	profession = ""
	j=0

	for line in txt.split('\n'):
		j+=1

		if j%3 == 2:
			ids=[int(a) for a in line.split(', ')]
		elif j%3 == 1:
			profession = line
		else:
			k=0
			print(profession)
			for id in ids:
				id = id - 2*id*(profession == "Enchanting")
				k+=1
				name, req, qual = get_item_by_number(id, profession)
				namedict[name] = id
				crafts_infos[id]={"Profession" : profession, "Req" : req, "Quality":qual}
				print(id, name, req, qual, profession, "(", k,	"/", len(ids),")")


	with open('namedict.pkl', 'wb') as f:
		pickle.dump(namedict, f, pickle.HIGHEST_PROTOCOL)

	with open('crafts_infos.pkl', 'wb') as f:
		pickle.dump(crafts_infos, f, pickle.HIGHEST_PROTOCOL)

	print('Namedict and crafts_infos created and saved')
	

#Create or update reqdict from wowhead
def create_reqdict():

	print('Creating reqdict')

	ids = []

	for i in crafts_infos.values():
		ids += list(i["Req"].keys())

	ids = list(set(ids)) #Remove duplicates
	nb=0

	for i in ids:
		nb+=1
		print('\rReq', nb, '/', len(ids), end='')
		if i not in reqdict.keys():
			name, _, _ = get_item_by_number(i, "")
			reqdict[i] = name

	print()

	with open('reqdict.pkl', 'wb') as f:
		pickle.dump(reqdict, f, pickle.HIGHEST_PROTOCOL)

	print('Reqdict created and saved')


#Check if a DB is ready for a specific server
def db_ready(servername):

	if not servername in crafts.keys():
		return False

	if not "LastUpdate" in crafts[servername].keys():
		return False

	if not crafts[servername]["LastUpdate"]:
		return False

	return True


#Main function
if __name__ == "__main__":

	load_datas()
	print('Pkl files loaded')

	client.run(open("botkey", "r").read())
	